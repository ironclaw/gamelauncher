#!/bin/bash

# BASH script to set video resolution for running a program a other than your current screen resolution and disable compositing during runtime and then return your screen resolution and compositing to current values when the program exits

# Ray Schwamberger,  1 February 2016

###############################################################################

# read in settings from the config file
if [ $# -ge 1 ]; then
        source ~/.config/gamelauncher/conf/${1}.conf
else
        echo "not a valid argument, please name configuration file in games conf directory"
        exit
fi

#set local variables

export PROGRAM_COMMAND
export WINEARCH
export WINEPREFIX
export WORKING_DIRECTORY
export PROGRAM_MODE
export COMPOSITE_TOGGLE
export WINEDEBUG

echo Program Name = ${PROGRAM_NAME}
echo WINEARCH = ${WINEARCH}
echo WINEPREFIX = ${WINEPREFIX}
echo Working Directory = ${WORKING_DIRECTORY}
echo Program Command = ${PROGRAM_COMMAND}
echo Video Mode = ${PROGRAM_MODE}
echo Composite Toggleing = ${COMPOSITE_TOGGLE}

# find out if KWIN is running and compositing is controllable
TESTKDE=`pgrep kwin`
if [ $? -eq 0 ] ; then
    echo "KDE is running"
    TESTKDE=true
else    
    TESTKDE=false
    echo "KDE is not running"
fi


if ${TESTKDE} == "true" ; then
    #KWIN is runing
    #get    current state of compositing in Kwin from dbus (true/false)
    KWIN_COMPOSITING_DEFAULT=$(qdbus org.kde.KWin /Compositor active)

    #check if compositing is enabled in Kwin and turn it off if it is enabled
    if ${COMPOSITE_TOGGLE} == "true" ; then
        if ${KWIN_COMPOSITING_DEFAULT} == "true" ; then
            echo KWin compositing is ${KWIN_COMPOSITING_DEFAULT}, disabling
            qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.suspend
            KWIN_TOGGLED="true"
        else
            KWIN_TOGGLED="false"
        fi
    fi
fi

#get current screen resolution information
SCREEN_NAME=`xrandr |grep " connected" | awk '{print $1}'`
SCREEN_LINE=`xrandr -q | grep Screen`
SCREEN_WIDTH=`echo ${SCREEN_LINE} | awk '{ print $8 }'`
SCREEN_HEIGHT=`echo ${SCREEN_LINE} | awk '{ print $10 }' | awk -F"," '{ print $1 }'`

echo Current screen resoluton on ${SCREEN_NAME} is ${SCREEN_WIDTH}x${SCREEN_HEIGHT}

#set screen resolution for the game output
echo Changing screen resolution on ${SCREEN_NAME} to ${PROGRAM_MODE}
xrandr --output ${SCREEN_NAME} --mode ${PROGRAM_MODE}


# Change to WORKING_DIRECTORY and execute PROGRAM_COMMAND
cd "${WORKING_DIRECTORY}"
${PROGRAM_COMMAND}
#reset the screen output back to preferred resolution for the desktop
echo Resetting screen resolution on ${SCREEN_NAME} TO ${SCREEN_WIDTH}x${SCREEN_HEIGHT}
xrandr --output ${SCREEN_NAME} --mode ${SCREEN_WIDTH}x${SCREEN_HEIGHT}

#check to see if compositing set to other than the default and toggle if it is
if ${TESTKDE} == "true" ; then
    if  ${KWIN_TOGGLED} == "true"  ; then
            echo KWin composting is $(qdbus org.kde.KWin /Compositor active), previous was ${KWIN_COMPOSITING_DEFAULT}, toggling
            qdbus org.kde.KWin /Compositor org.kde.kwin.Compositing.resume
    else
        :
    fi

fi
