#!/bin/bash

if [ $# -ge 1 ]; then
        OUTPUT_FILE=${HOME}"/.config/gamelauncher/conf/"${1}".conf"
else
        echo "not a valid argument, please name configuration file"
        exit
fi


#PROGRAM_NAME

PROGRAM_NAME=$(kdialog --inputbox "Please enter the friendly name of the program" "Program");

case "$?" in
    0)
        ;;
    1)
        kdialog --sorry "You chose to cancel, setting to null";
        PROGRAM_NAME=""
        ;;
    *)
        kdialog --error "ERROR";
        ;;
esac;

#PROGRAM_COMMAND

PROGRAM_COMMAND=$(kdialog --inputbox "Please enter the command that starts the program" "command");

case "$?" in
    0)
        ;;
    1)
        kdialog --sorry "You chose to cancel setting to null";
        PROGRAM_COMMAND=""
        ;;
    *)
        kdialog --error "ERROR";
        ;;
esac;

#WINEARCH

WINEARCH=$(kdialog --combobox "Please choose the wine architechture:" "win32" "win64" --default "win32");

if [ "$?" = 0 ] ; then
    if [ "${WINEARCH}" = win32 ]; then
        break;
    elif [ "${WINEARCH}" = win64 ]; then
        break;
    else
        kdialog --msgbox "You did not choose, setting to null";
        WINEARCH=""
    fi;
elif [ "$?" = 1 ]; then
    kdialog --sorry "You chose to cancel, setting to null";
    WINEARCH=""
else
    kdialog --error "ERROR";
fi;

#WINEPREFIX

WINEPREFIX=$(kdialog --title "Please choose your wine prefix directory" --getexistingdirectory *);

case "$?" in
	0)
		;;
	1)
		kdialog --sorry "You chose to cancel, setting to null";
		WINEPREFIX=""
		;;
	*)
		kdialog --error "ERROR";
		;;
esac;

#WINEDEBUG

WINEDEBUG=$(kdialog --combobox "Do you wish to supress wine debugging?" "true" "false" --default "false");

if [ "$?" = 0 ]; then
	if [ "${WINEDEBUG}" = true ]; then
		WINEDEBUG="-all"
	elif [ "${WINEDEBUG}" = false ]; then
		WINEDEBUG=""
	else
		kdialog --sorry "You did not choose, not supressing";
		WINEDEBUG=""
	fi;
elif [ "$?" = 1 ]; then
	kdialog --sorry "You chose to cancel, not surpessing";
        WINEDEBUG=""
else
	kdialog --error "ERROR";
fi;


#WORKING_DIRECTORY

WORKING_DIRECTORY=$(kdialog --title "Please choose your working directory" --getexistingdirectory *);

case "$?" in
	0)
		;;
	1)
		kdialog --sorry "You chose To cancel, setting to null";
		WORKING_DIRECTORY=""
		;;
	*)
		kdialog --error "ERROR";
		;;
esac;


#PROGRAM_MODE

PROGRAM_MODE=$(kdialog --inputbox "Please enter the graphics mode (WxH)" "640x480");

case "$?" in
    0)
        ;;
    1)
        kdialog --sorry "You chose to cancel, setting to null";
        PROGRAM_MODE=""
        ;;
    *)
        kdialog --error "ERROR";
        ;;
esac;

#COMPOSITE_TOGGLE

COMPOSITE_TOGGLE=$(kdialog --combobox "Do you wish to turn off desktop compositing for this program?" "true" "false" --default "false");

case "$?" in
	0)
		case "${COMPOSITE_TOGGLE}" in
                        true)
                                ;;
			false)
                                ;;
			*)
				kdialog --sorry "You did not choose, setting to false";
                                COMPOSITE_TOGGLE="false"
				;;
		esac;
		;;
	1)
		kdialog --sorry "You chose cancel, setting to false";
		COMPOSITE_TOGGLE="false"
		;;
	*)
		kdialog --error "ERROR";
		;;
esac;

   
echo "Program name: " ${PROGRAM_NAME}
echo "Program command: " ${PROGRAM_COMMAND}
echo "Wine architechture: " ${WINEARCH}
echo "Wine prefix: " ${WINEPREFIX}
echo "Wine debugging: " ${WINEDEBUG}
echo "Working directory: " ${WORKING_DIRECTORY}
echo "Video mode: " ${PROGRAM_MODE}
echo "Composite toggling: " ${COMPOSITE_TOGGLE}

touch ${OUTPUT_FILE}
echo "#PROGRAM_NAME is the friendly name of the program" > ${OUTPUT_FILE}
echo "PROGRAM_NAME="'"'${PROGRAM_NAME}'"' >> ${OUTPUT_FILE}
echo "" >> ${OUTPUT_FILE}

echo "#PROGRAM_COMMAND is the command that starts the program" >> ${OUTPUT_FILE}
echo "PROGRAM_COMMAND="'"'${PROGRAM_COMMAND}'"' >> ${OUTPUT_FILE}
echo "" >> ${OUTPUT_FILE}

echo "#WINEARCH is the architecture wine should use" >> ${OUTPUT_FILE}
echo "WINEARCH="'"'${WINEARCH}'"' >> ${OUTPUT_FILE}
echo "" >> ${OUTPUT_FILE}

echo "#WINEPREFIX is the directory containing the wine prefix" >> ${OUTPUT_FILE}
echo "WINEPREFIX="'"'${WINEPREFIX}'"' >> ${OUTPUT_FILE}
echo "" >> ${OUTPUT_FILE}

echo "#WINEDEBUG is the WINE debugging variable" >> ${OUTPUT_FILE}
echo "WINEDEBUG="'"'${WINEDEBUG}'"' >> ${OUTPUT_FILE}
echo "" >> ${OUTPUT_FILE}

echo "#WORKING_DIRECTORY is the directory to launch the program from" >> ${OUTPUT_FILE}
echo "WORKING_DIRECTORY="'"'${WORKING_DIRECTORY}'"' >> ${OUTPUT_FILE}
echo "" >> ${OUTPUT_FILE}

echo "#PROGRAM_MODE is the video mode to use" >> ${OUTPUT_FILE}
echo "PROGRAM_MODE="'"'${PROGRAM_MODE}'"' >> ${OUTPUT_FILE}
echo "" >> ${OUTPUT_FILE}

echo "#COMPOSITE_TOGGLE controls toggling compositing during runtime, true toggles, false does not" >> ${OUTPUT_FILE}
echo "COMPOSITE_TOGGLE="'"'${COMPOSITE_TOGGLE}'"' >> ${OUTPUT_FILE}
echo "" >> ${OUTPUT_FILE}
